/* eslint-disable react-hooks/rules-of-hooks */
import Layout from "@/components/Layouts/Layout";
import { useRouter } from "next/router";
import data from "@/components/Data";
import Image from "next/image";
import classNames from "classnames";
import Modal from "@/components/Tools/Modal";
import { useState } from "react";
export default function Category() {
  const router = useRouter();
  const { id } = router.query;
  const dataFood = data?.sections.find((a) => a.id == id);
  if (!id) {
    return;
  }
  const [selected, setSelected] = useState(null);

  const { locale } = router;
  return (
    <Layout>
      <Modal selected={selected} setSelected={setSelected} />
      <section className="grid md:grid-cols-12 grid-cols-1 my-10 px-4 gap-5">
        {dataFood.data.map((item) => (
          <div
            onClick={() => setSelected(item)}
            className={classNames(
              "relative md:h-[400px] h-[250px] overflow-hidden rounded-lg cursor-pointer",
              {
                "md:col-span-7 col-span-full": item.id % 2 != 0,
                "md:col-span-5 col-span-full": item.id % 2 == 0,
              }
            )}
            key={item.id}
          >
            <div className="background absolute h-full w-full rounded-lg overflow-hidden z-50"></div>
            <Image src={item.img} alt="" className="w-full" fill />
            <div className="absolute md:top-4 md:left-4 left-2 top-2 z-50">
              <p className="font-normal">
                {locale === "en" ? item.category : item.categoryAr}
              </p>
              <h3 className=" font-medium md:text-5xl text-2xl">
                {" "}
                {locale === "en" ? item.name : item.nameAr}
              </h3>
            </div>
            <div className="absolute md:bottom-4 md:right-4 right-2 bottom-2 z-50">
              <p className="md:text-5xl text-2xl font-semibold">{item.price}</p>
            </div>
          </div>
        ))}
      </section>
    </Layout>
  );
}
