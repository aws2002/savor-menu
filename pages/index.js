import { Element } from "react-scroll";
import Image from "next/image";
import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { AiOutlineArrowRight } from "react-icons/ai";
import data from "@/components/Data";
import Layout from "@/components/Layouts/Layout";
import Link from "next/link";
import Category from "@/components/SectionsPages/Category";
import Modal from "@/components/Tools/Modal";
import { motion } from "framer-motion";
import classNames from "classnames";
import { useRouter } from "next/router";

export default function Home() {
  const [selected, setSelected] = useState(null);
  const router = useRouter();
  const { locale } = router;
  return (
    <Layout>
      <Category />
      <Modal selected={selected} setSelected={setSelected} />
      {data?.sections.map(({ id, name, data,nameAr}) => (
        <Element name={id} className="my-5 pl-4" key={id}>
          <div className=" flex justify-between items-center">
            <h2 className="md:text-3xl text-xl text-[#C6C0AE]">{locale === "en" ? name :nameAr}</h2>
            <Link href={`category/${id}`} className="pr-4">
              <AiOutlineArrowRight className=" text-lg text-[#C6C0AE]" />
            </Link>
          </div>
          <Swiper
            slidesPerView={1.4}
            spaceBetween={8}
            pagination={{
              clickable: true,
            }}
            className="mySwiper mt-2"
          >
            {data?.slice(0, 4).map((item) => (
              <Swiper
                key={id}
                slidesPerView={1.4}
                spaceBetween={8}
                pagination={{
                  clickable: true,
                }}
                className="mySwiper mt-2"
              >
                <SwiperSlide
                  onClick={() => {
                    setSelected(item);
                  }}
                  key={id}
                  className={classNames("rounded-lg overflow-hidden")}
                >
                  <motion.div
                    whileHover={{
                      scale: 1.01,
                      transition: {
                        duration: 0.2,
                      },
                    }}
                    whileTap={{
                      scale: 0.95,
                    }}
                    className="relative md:h-[400px] h-40 cursor-pointer"
                  >
                    <div className="background absolute h-full w-full rounded-lg overflow-hidden z-50"></div>
                    <Image src={item.img} alt="" className="w-full" fill />
                    <div className="absolute md:top-4 md:left-4 left-2 top-2 z-50">
                      <p className="font-normal"> {locale === "en" ? item.category : item.categoryAr}</p>
                      <h3 className=" font-medium md:text-5xl text-2xl text-[#C6C0AE]">
                        {locale === "en" ? item.name : item.nameAr}
                      </h3>
                    </div>
                    <div className="absolute md:bottom-4 md:right-4 right-2 bottom-2 z-50">
                      <p className="md:text-5xl text-2xl font-semibold">
                        {item.price} SAR
                      </p>
                    </div>
                  </motion.div>
                </SwiperSlide>
              </Swiper>
            ))}
          </Swiper>
        </Element>
      ))}
    </Layout>
  );
}
