import "@/styles/globals.css";
import "swiper/css";
import "swiper/css/pagination";
import { useRouter } from "next/router";
import { AnimatePresence } from "framer-motion";
export default function App({ Component, pageProps }) {
  const router = useRouter();
  return (
    <AnimatePresence mode="wait" initial={false}>
      <Component {...pageProps} key={router.asPath} />;
    </AnimatePresence>
  );
}
