import { useState } from "react";
import Image from "next/image";
import classNames from "classnames";
export default function ImageLoading({
  src = "",
  width = 0,
  height = 0,
  alt = "",
  className = "",
  style = {},
}) {
  const [isLoading, setLoading] = useState(true);
  return (
    <Image
      src={src}
      width={width}
      height={height}
      loading="lazy"
      quality={100}
      style={style}
      alt={alt}
      className={classNames(
        "duration-700 ease-in-out user-select",
        isLoading
          ? "grayscale blur-lg scale-110"
          : "grayscale-0 blur-0 scale-100",
        className
      )}
      onLoadingComplete={() => setLoading(false)}
    />
  );
}
