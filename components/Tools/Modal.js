/* eslint-disable react/display-name */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useRef, memo } from "react";
import { motion, useMotionValue, useTransform } from "framer-motion";
import { useScrollConstraints } from "../utils/use-scroll-constraints";
import { closeSpring, openSpring } from "../utils/animations";
import Image from "next/image";
import { useRouter } from "next/router";
const dismissDistance = 200;

const Modal = memo(({ selected, setSelected }) => {
  if (!selected) return;
  const y = useMotionValue(0);
  const opacity = useTransform(y, [0, 200], [1, 0]);
  const cardRef = useRef(null);
  const constraints = useScrollConstraints(cardRef, selected);
  console.log(selected);
  const router = useRouter();
  const { locale } = router;
  return (
    <motion.div
      onClick={() => setSelected(null)}
      className="fixed inset-0 bg-black/50 z-[1000] overflow-y-scroll modall"
      style={{ opacity }}
    >
      <motion.div
        ref={cardRef}
        className="w-full max-w-[600px] mx-auto mt-24 px-5 rounded-2xl cursor-grab"
        drag={selected ? "y" : false}
        dragConstraints={constraints}
        layoutTransition={selected ? openSpring : closeSpring}
        onDrag={(event, info) => {
          if (info.offset.y > dismissDistance) {
            setSelected(null);
          }
        }}
        style={{ y }}
      >
        <motion.div
          layoutId={`card-${selected.id}`}
          className="bg-[#782C4A] pb-4 rounded-xl overflow-hidden"
        >
          <motion.div transition={closeSpring}>
            <div className="items-center relative">
              
              <div className="overflow-hidden rounded-lg w-full h-72">
                <Image fill  src={selected.img} alt={""}  className="w-full h-full"/>
              </div>
              
            </div>
            <div className=" px-4 flex justify-between mt-4">
                <h2 className=" text-[#ffffff] font-medium text-xl">
                {locale === "en" ? selected.name : selected.nameAr}
           
                </h2>
                <span className="text-[#ffffff] mt-1 mb-2 font-normal">
                  {selected.price} SAR
                </span>
              </div>
            <p className="text-white font-light md:text-base text-sm mt-3 px-4">
         
              {locale === "en" ?selected.briefDescription: selected.briefDescriptionAr}
            </p>
          </motion.div>
        </motion.div>
      </motion.div>
    </motion.div>
  );
});

export default Modal;
