import React from "react";
import { Link } from "react-scroll";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import data from "@/components/Data";
import { useRouter } from "next/router";
export default function Category() {
  const router = useRouter();
  const { locale } = router;
  return (
    <section className="my-4 pl-4">
      <div>
        <h2 className="font-medium md:text-4xl text-2xl mb-4 text-[#C6C0AE]">Category</h2>
        <Swiper
          slidesPerView={4.4}
          spaceBetween={8}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            640: {
              slidesPerView: 4.4,
            },
            768: {
              slidesPerView: 8.4,
            },
            1024: {
              slidesPerView: 8.4,
            },
          }}
          className="mySwiper mt-2"
        >
          {data.category.map(({ id, img, name,nameAr }) => (
            <SwiperSlide key={id}>
              <Link
                spy={true}
                smooth={true}
                duration={100}
                to={id}
                delay={100}
                offset={-80}
                className="relative"
              >
                <Image src={img} width={140} alt="" height={0} className="w-full"/>
                <div className="text-center mt-1">
                  <p className=" md:text-lg text-sm text-[#C6C0AE] font-normal md:px-3 px-1 rounded-full">
                  {locale === "en" ? name : nameAr}
                  </p>
                </div>
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </section>
  );
}
