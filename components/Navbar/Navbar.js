import Image from "next/image";
import Link from "next/link";
import { FaFacebookF } from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";
import { BiMap } from "react-icons/bi";
import { BsFillTelephoneFill, BsSnapchat } from "react-icons/bs";
import { CONSTANT } from "@/constants";
import { useRouter } from "next/router";
export default function Navbar() {
  const router = useRouter();
  const { locale } = router;
  const changeLanguage = (e) => {
    const locale = e.target.value;
    router.push(router.pathname, router.asPath, { locale });
  };
  return (
    <>
    <div className="px-4 mt-4 justify-end flex">
    <select
        onChange={changeLanguage}
        defaultValue={locale}
        className="text-shadow-sm text-lg bg-transparent tracking-wide text-[#C6C0AE]"
      >
        <option className="text-black" value="en">
          EN
        </option>
        <option className="text-black" value="ar">
          Ar
        </option>
      </select>
    </div>
     
      <header className="px-4 mt-4 flex justify-between items-center">
        <Link href="/" className="bg-white block w-28 py-1 px-3 rounded-full">
          <Image alt="" width={100} height={0} src={"/assets/logo.png"} />
        </Link>
        <div className="flex gap-3">
          <Link href={CONSTANT.INSTAGRAM} className="md:p-3 p-2 rounded-full bg-[#782C4A]">
            <AiFillInstagram className="md:text-lg text-base text-[#C6C0AE]" />
          </Link>
          <Link href={CONSTANT.MAP} className="md:p-3 p-2 rounded-full bg-[#782C4A]">
            <BiMap className="md:text-lg text-base text-[#C6C0AE]" />
          </Link>
          <Link href={CONSTANT.SNAPCHAT} className="md:p-3 p-2 rounded-full bg-[#782C4A]">
            <BsSnapchat className="md:text-lg text-base text-[#C6C0AE]" />
          </Link>
          <Link href={CONSTANT.MOBILE} className="md:p-3 p-2 rounded-full bg-[#782C4A]">
            <BsFillTelephoneFill className="md:text-lg text-base text-[#C6C0AE]" />
          </Link>
        </div>
      </header>
    </>
  );
}
